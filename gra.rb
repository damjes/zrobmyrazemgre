require 'json'
require 'gosu'
require 'matrix'
require 'set'

class SortedSet
	def shift
		e = self.first
		self.delete self.first
		return e
	end
end

class Visited
	attr_accessor :droga, :x, :y, :odl, :heu, :suma

	def initialize x, y, xcel, ycel, droga = Array.new
		@droga = droga
		@x = x
		@y = y
		@odl = droga.length
		@heu = (xcel - x).abs + (ycel - y).abs
		@suma = @odl + @heu
	end

	def <=> inny
		@suma <=> inny.suma
	end
end

class Plytka
	def initialize okno
		@okno = okno
	end
	
	def draw x, y, z = 0
		unless @obrazek.nil?
			wsp_ekranu = Matrix[[x, y]] * @okno.macierz
			@obrazek.draw(wsp_ekranu[0, 0], wsp_ekranu[0, 1], z)
		end
	end
	
	def moge_wejsc?
		true
	end
	
	def zbieralny?
		false
	end
end

class Gracz < Plytka
	attr_accessor :x, :y, :wynik
	
	def initialize okno, x, y
		super okno
		@x = x
		@y = y
		@wynik = 0
		@obrazek = Gosu::Image.new(@okno, 'images/gracz.png', false)
		@ten_ruch_x = @ten_ruch_y = 0
		@deltax = @deltay = 0
	end
	
	def draw_me
		draw @x, @y, 1
	end
	
	def przesun deltax, deltay
		nowex = @x + deltax
		nowey = @y + deltay
		
		if @okno.mapa[nowey][nowex].moge_wejsc?
			@okno.gracz_wszedl nowex, nowey
			
			@ten_ruch_x = deltax
			@ten_ruch_y = deltay
		end
	end
	
	def update
		@deltax += @ten_ruch_x
		@deltay += @ten_ruch_y
		
		@x += @ten_ruch_x / 15.0
		@y += @ten_ruch_y / 15.0
		
		@deltax %= 15
		@deltay %= 15
		
		if @deltax == 0 and @deltay == 0
			@x = @x.round
			@y = @y.round
			@ten_ruch_x = @ten_ruch_y = 0
			
			przesun(-1,  0) if @okno.button_down? Gosu::KbLeft
			przesun( 1,  0) if @okno.button_down? Gosu::KbRight
			przesun( 0, -1) if @okno.button_down? Gosu::KbUp
			przesun( 0,  1) if @okno.button_down? Gosu::KbDown
		end
	end

	def idz x, y
		do_odw = SortedSet.new [Visited.new @x, @y, x, y]
		hasz = {[@x, @y] => do_odw.first}
		while not do_odw.empty?
			wezel = do_odw.shift
			return wezel.droga if wezel.x == x and wezel.y == y
			droga = wezel.droga
			droga << [wezel.x, wezel.y]
			lista_sasiadow = [
				Visited.new(wezel.x-1, wezel.y, x, y, droga),
				Visited.new(wezel.x+1, wezel.y, x, y, droga),
				Visited.new(wezel.x, wezel.y-1, x, y, droga),
				Visited.new(wezel.x, wezel.y+1, x, y, droga)
				]

			maxy = @okno.mapa.length
			maxx = @okno.mapa.first.length
			lista_sasiadow.filter! do |e|
				e.x >= 0 and
					e.x =< maxx and
					e.y >= 0 and
					e.y =< maxy and
					@okno.mapa[e.y][e.x].moge_wejsc?
			end

			lista.sasiadow.each do |e|
				if not hasz.has_key? [[e.x, e.y]] or e.heu < hasz[[e.x, e.y]].heu
					hasz[[e.x, e.y]] = e
					do_odw << e
				end
			end
		end
	end
end

class Gwiazdka < Plytka
	def initialize okno
		super okno
		@obrazki = Gosu::Image.load_tiles(@okno, 'images/gwiazdka.png', 16, 16, false)
		@klatka = rand 30
	end
	
	def draw x, y, z = 0
		@obrazek = @obrazki[@klatka]
		
		super x, y, z
		
		@klatka += rand 2
		@klatka %= 30
	end
	
	def zbieralny?
		true
	end
end

class Sciana < Plytka
	def initialize okno
		super okno
		@obrazek = Gosu::Image.new(@okno, 'images/sciana.png', false)
	end
	
	def moge_wejsc?
		false
	end
end

class Okno < Gosu::Window
	attr_reader :macierz, :mapa
	
	def initialize
		super 800, 600, false
		@macierz = Matrix[[16, 0], [0, 16]]
		@font = Gosu::Font.new(self, 'Tahoma', 20)

		wczytaj
	end
		
	def wczytaj
		level = JSON.load File.new 'level.json'
		
		@gracz = Gracz.new self, level['gracz'][0], level['gracz'][1]
		
		@mapa = Array.new
		level['mapa'].each do |wiersz|
			@mapa << Array.new
			wiersz.each do |pole|
				case pole
					when 'g' then @mapa.last << Gwiazdka.new(self)
					when 's' then @mapa.last << Sciana.new(self)
					when 'p' then @mapa.last << Plytka.new(self)
				end
			end
		end
	end

	def draw
		@mapa.each_index do |y|
			@mapa[y].each_index do |x|
				obiekt = @mapa[y][x]
				obiekt.draw x, y
			end
		end
		
		@gracz.draw_me

		@font.draw @gracz.wynik, 10, 10, 3, 1, 1, Gosu::Color::YELLOW
	end

	def button_down id
		close if id == Gosu::KbEscape
=begin
		@gracz.przesun(-1,  0) if id == Gosu::KbLeft or id == Gosu::Gp1Left
		@gracz.przesun( 1,  0) if id == Gosu::KbRight or id == Gosu::Gp0Right
		@gracz.przesun( 0, -1) if id == Gosu::KbUp or id == Gosu::Gp2Up
		@gracz.przesun( 0,  1) if id == Gosu::KbDown or id == Gosu::Gp0Down
=end
		wczytaj if id == Gosu::KbR
	end
	
	def gracz_wszedl x, y
		if @mapa[y][x].zbieralny?
 			@mapa[y][x] = Plytka.new(self)
 			@gracz.wynik += 1
	 	end
	end
	
	def update
		@gracz.update
	end
end

Okno.new.show
